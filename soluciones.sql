﻿/**
  Ejemplos 1
  Consultas de seleccion
  Son un conjunto de consultas con totales y seleccion
  Vamos a trabajar con where, having y group by
**/

USE concursos;

/* 1.1	Indicar el 
  nombre de los concursantes de burgos  
*/

-- solucion 1
SELECT DISTINCT
  c.nombre
FROM
  concursantes c
WHERE
  c.provincia LIKE '%Burgos%';

-- solucion 2
SELECT DISTINCT
  c.nombre
FROM
  concursantes c
WHERE
  c.provincia='burgos';


/* 1.2	Indicar cuantos concursantes hay de burgos  */

-- solucion 1
SELECT 
  COUNT(*) numConcursantes 
FROM 
  concursantes c 
WHERE 
  c.provincia='Burgos';

/* 1.3	Indicar cuantos concursantes hay de cada poblacion */
  
-- SOLUCION 1
SELECT 
  c.poblacion,COUNT(*) 
 FROM 
  concursantes c 
 GROUP BY 
  c.poblacion;

/* 1.4	Indicar las poblaciones que tienen 
concursantes de menos de 90kg */

-- solucion 
SELECT DISTINCT 
  c.poblacion
FROM concursantes c
WHERE c.peso < 90;

/* 1.5	Indicar las poblaciones que 
  tienen mas de 1 concursante 
*/

-- solucion 1
SELECT poblacion
      FROM concursantes
      GROUP BY poblacion
      HAVING COUNT(*)>1;

-- solucion 2
SELECT c1.poblacion FROM (
SELECT poblacion,COUNT(*) nConcursantes 
  FROM concursantes c 
  GROUP BY c.poblacion) c1
  WHERE c1.nConcursantes>1;


/* 1.6	Indicar las poblaciones 
  que tienen mas de 1 concursante de menos de 90 kg 
*/

 SELECT c.poblacion
FROM concursantes c 
WHERE c.peso<90  
GROUP BY c.poblacion
HAVING COUNT(*)>1;  


/* 1.7	Listar nombre y poblacion de los concursantes 
  que hayan nacido en los meses de Julio, Agosto y Septiembre
*/

  SELECT c.nombre,c.poblacion 
    FROM concursantes c 
    WHERE MONTH(c.fechaNacimiento) IN (7,8,9);

/* 1.8	En que años han nacido mas de 2 concursantes */

  SELECT YEAR(c.fechaNacimiento) 
    FROM concursantes c GROUP BY YEAR(c.fechaNacimiento)
    HAVING COUNT(*)>2;

/* 1.9	Provincia cuya altura media de los varones (nombre no termina en a) supera el 1,75. */

  SELECT c.provincia FROM concursantes c 
    WHERE RIGHT(RTRIM(c.nombre),1)!='a'
    GROUP BY c.provincia HAVING AVG(c.altura)<175;

/* 1.10	Lista las provincias cuyo peso total de todos los concursantes femeninas supera los 300 kg */

  SELECT c.provincia FROM concursantes c 
    WHERE RIGHT(RTRIM(c.nombre),1)='a'
    GROUP BY c.provincia 
    HAVING SUM(c.peso)>300;

/* 1.11	Cuantos hombres hay */

  SELECT COUNT(*) FROM concursantes c 
    WHERE RIGHT(RTRIM(c.nombre),1)!='a';

-- 1.12 Indicar el nombre de los concursantes cuya altura sea 170cm o mas
  
  SELECT DISTINCT c.nombre 
    FROM concursantes c 
    WHERE c.altura>=170; 


-- 1.13 indicar los pesos que se repiten mas de una vez
  
  -- con having
  SELECT c.peso FROM concursantes c 
    GROUP BY c.peso 
    HAVING COUNT(*)>1; 
  
  -- sin having
  SELECT c1.peso FROM (
    SELECT c.peso,COUNT(*) n FROM concursantes c 
    GROUP BY c.peso 
    ) c1
    WHERE n>1;


  -- 1.14 indicar las poblaciones con mas de 3 concursantes que hayan nacido despues de 1983
    
    SELECT c.poblacion FROM concursantes c 
    WHERE YEAR(c.fechaNacimiento)>1983 
    GROUP BY c.poblacion 
    HAVING COUNT(*)>3;



-- 1.15 Indicar la altura media y el peso maximo de los concursantes por provincia

    SELECT c.provincia, AVG( c.altura) alturaMedia, MAX( c.peso) pesoMaximo 
    FROM concursantes c 
    GROUP BY c.provincia; 

-- 1.16 Indicar la provincia que tiene mas de 5 concursantes de peso entre 95 y 105 y mostrar tambien el peso maximo y minimo

  SELECT c.provincia, MAX(c.peso), MIN(c.peso) 
    FROM concursantes c 
    WHERE c.peso BETWEEN 95 AND 105 
    GROUP BY c.provincia 
    HAVING COUNT(*)>=5; 

